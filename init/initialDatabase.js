// 初始数据库
const { TABLE } = require('../utils/constants')
const cryptoPwd = require('../utils/crypto')

const { salt, key } = cryptoPwd('123456')

const now = Date.now()

module.exports = [
  {
    name: TABLE.USER,
    data: [
      {
        id: 1,
        account: 'admin',
        salt,
        password: key,
        realName: '超级管理员',
        roles: [],
        status: 1,
        isDel: 0,
        _lastIp: '0.0.0.0',
        _lastTime: now,
        _addTime: now,
        _updateTime: now
      }

    ]
  },
  {
    name: TABLE.ROLE,
    data: []
  },
  {
    name: TABLE.MENU,
    data: [{ id: 1, pid: 0, name: '商品', icon: 'apple', params: '', path: '/product', uniqueAuth: '', sort: 1, isHidden: 0, status: 1, isDel: 0, _addTime: now, _updateTime: now, order: 1 }, { id: 2, pid: 1, name: '商品列表', icon: 'coin', params: '', path: '/product/list', uniqueAuth: 'product_list', sort: 1, isHidden: 0, status: 1, isDel: 0, _addTime: now, _updateTime: now, order: 1 }, { id: 3, pid: 1, name: '商品分类', icon: 'bowl', params: '', path: '/product/category', uniqueAuth: 'product_classify', order: 2, isHidden: 0, status: 1, isDel: 0, _addTime: now, _updateTime: now }, { id: 4, pid: 1, name: '商品规格', icon: 'brush', params: '', path: '/product/attr', uniqueAuth: 'product_attr', order: 3, isHidden: 0, status: 1, isDel: 0, _addTime: now, _updateTime: now }, { id: 5, pid: 1, name: '商品评论', icon: 'chat-line-round', params: '', path: '/product/reply', uniqueAuth: 'product_reply', order: 4, isHidden: 0, status: 1, isDel: 0, _addTime: now, _updateTime: now }, { id: 6, pid: 0, name: '权限管理', icon: 'cold-drink', params: '', path: '/permission', uniqueAuth: '', order: 2, isHidden: 0, status: 1, isDel: 0, _addTime: now, _updateTime: now }, { id: 7, pid: 6, name: '管理员', icon: 'user', params: '', path: '/permission/admin', uniqueAuth: 'permission_admin', order: 1, isHidden: 0, status: 1, isDel: 0, _addTime: now, _updateTime: now }, { id: 8, pid: 6, name: '角色', icon: 'smoking', params: '', path: '/permission/role', uniqueAuth: 'permission_role', order: 2, isHidden: 0, status: 1, isDel: 0, _addTime: now, _updateTime: now }, { id: 9, pid: 6, name: '菜单', icon: 'Menu', params: '', path: '/permission/menu', uniqueAuth: 'permission_menu', order: 3, isHidden: 0, status: 1, isDel: 0, _addTime: now, _updateTime: now }]
  },
  {
    name: TABLE.CATEGORY,
    data: []
  }
]
