const { body } = require('express-validator')
const { validResultCallback } = require('../middleware')

// 登录校验
const loginValidator = [
  body('imgcode')
    .notEmpty()
    .withMessage('验证码未传'),
  body('account')
    .notEmpty()
    .withMessage('用户名未传'),
  body('pwd')
    .notEmpty()
    .withMessage('密码未传'),
  validResultCallback
]

module.exports = {
  loginValidator
}
