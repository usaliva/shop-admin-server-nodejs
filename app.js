// app.js
const express = require('express')
const path = require('path')
const morgan = require('morgan')
const cors = require('cors')
const router = require('./router')
const session = require('express-session')
const { sessionSecret, serverPort } = require('./config/config.default')

const app = express()

// session 中间件
app.use(session({
  secret: sessionSecret, // 签发 Session ID 的密钥
  resave: false,
  saveUninitialized: true,
  cookie: {
    httpOnly: true,
    maxAge: 1000 * 6 // 过期时间（毫秒）
  }
}))
// 日志中间件
app.use(morgan('dev'))
// 跨域中间件
app.use(cors())
// 解析 application/json 请求体
app.use(express.json())
// 解析 application/x-www-form-urlencoded 请求体
app.use(express.urlencoded({ extended: true }))

// 静态资源路径
app.use(express.static(path.join(__dirname, './public')))

// 注册路由
app.use(router)

const port = process.env.PORT || serverPort || 5000

app.listen(port, () => {
  console.log(`server is running on http://localhost:${port}`)
})
