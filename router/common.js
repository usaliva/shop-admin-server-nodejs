// router\common.js
/* 基础接口 */
const express = require('express')
const router = express.Router()
const { tokenParser } = require('../middleware')
const controller = require('../controller/common')
const { loginValidator } = require('../validator/common')

// 测试
router.get('/demo', controller.demo)

// 测试 解析token
router.get('/token', tokenParser, controller.token)

// 登录验证码
router.get('/captcha', controller.captcha)

// 管理员登录
router.post('/login', loginValidator, controller.login)

module.exports = router
