const express = require('express')
const router = express.Router()
const { tokenParser, idValidator, isAdmin } = require('../../middleware')
const controller = require('../../controller/setting/role')
const validator = require('../../validator/setting/role')

// 获取角色列表
router.get('/', tokenParser, isAdmin, validator.getRolesValidator, controller.getRoles)

// 添加角色
router.post('/', tokenParser, isAdmin, validator.createRoleValidator, controller.createRole)

// 修改角色
router.put('/:id', tokenParser, isAdmin, idValidator, validator.updateRoleValidator, controller.updateRole)

// 获取角色
router.get('/:id', tokenParser, isAdmin, idValidator, controller.getRole)

// 删除角色
router.delete('/:id', tokenParser, isAdmin, idValidator, controller.deleteRole)

// 修改角色状态
router.put('/:id/set_status/:status', tokenParser, isAdmin, idValidator, validator.statusValidator, controller.setRoleStatus)

module.exports = router
