const express = require('express')
const router = express.Router()

router.use('/admin', require('./admin'))
router.use('/role', require('./role'))
router.use('/menu', require('./menu'))

module.exports = router
