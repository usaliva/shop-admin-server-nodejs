const express = require('express')
const router = express.Router()
const { tokenParser, idValidator, isAdmin } = require('../../middleware')
const controller = require('../../controller/setting/menu')
const validator = require('../../validator/setting/menu')

// 获取菜单树形列表
router.get('/', tokenParser, isAdmin, validator.getMenusValidator, controller.getMenus)

// 添加菜单
router.post('/', tokenParser, isAdmin, validator.createMenuValidator, controller.createMenu)

// 修改菜单
router.put('/:id', tokenParser, isAdmin, idValidator, validator.updateMenuValidator, controller.updateMenu)

// 获取菜单
router.get('/:id', tokenParser, isAdmin, idValidator, controller.getMenu)

// 删除菜单
router.delete('/:id', tokenParser, isAdmin, idValidator, controller.deleteMenu)

// 修改菜单状态
router.put('/:id/set_status/:status', tokenParser, isAdmin, idValidator, validator.statusValidator, controller.setMenuStatus)

module.exports = router
