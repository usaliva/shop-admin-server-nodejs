const express = require('express')
const router = express.Router()
const { tokenParser, idValidator, isAdmin } = require('../../middleware')
const controller = require('../../controller/setting/admin')
const validator = require('../../validator/setting/admin')

// 获取管理员列表
router.get('/', tokenParser, isAdmin, validator.getAdminsValidator, controller.getAdmins)

// 添加管理员
router.post('/', tokenParser, isAdmin, validator.createAdminValidator, controller.createAdmin)

// 修改管理员
router.put('/:id', tokenParser, isAdmin, idValidator, validator.updateAdminValidator, controller.updateAdmin)

// 获取管理员
router.get('/:id', tokenParser, isAdmin, idValidator, controller.getAdmin)

// 删除管理员
router.delete('/:id', tokenParser, isAdmin, idValidator, controller.deleteAdmin)

// 修改管理员状态
router.put('/:id/set_status/:status', tokenParser, isAdmin, idValidator, validator.statusValidator, controller.setAdminStatus)

// 修改管理员密码
router.put('/:id/set_password', tokenParser, isAdmin, idValidator, validator.pwdValidator, controller.setAdminPassword)

module.exports = router
