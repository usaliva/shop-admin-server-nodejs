const express = require('express')
const router = express.Router()
const { tokenParser, idValidator, isAdmin } = require('../../middleware')
const controller = require('../../controller/product/category')
const validator = require('../../validator/product/category')

// 获取分类树形列表
router.get('/', tokenParser, isAdmin, validator.getCategorysValidator, controller.getCategorys)

// 添加分类
router.post('/', tokenParser, isAdmin, validator.createCategoryValidator, controller.createCategory)

// 修改分类
router.put('/:id', tokenParser, isAdmin, idValidator, validator.updateCategoryValidator, controller.updateCategory)

// 获取分类
router.get('/:id', tokenParser, isAdmin, idValidator, controller.getCategory)

// 删除分类
router.delete('/:id', tokenParser, isAdmin, idValidator, controller.deleteCategory)

// 修改分类状态
router.put('/:id/set_status/:status', tokenParser, isAdmin, idValidator, validator.statusValidator, controller.setCategoryStatus)

module.exports = router
