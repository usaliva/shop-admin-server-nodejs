// router\index.js
const express = require('express')
const router = express.Router()
const { errorHandler } = require('../middleware')

router.use('/api/admin', require('./common'))
router.use('/api/admin/setting', require('./setting'))
router.use('/api/admin/product', require('./product'))

// 统一错误处理中间件
router.use(errorHandler)

module.exports = router
