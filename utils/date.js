const dayjs = require('dayjs')

const FMT = 'YYYY-MM-DD HH:mm:ss'

exports.format = time => dayjs(time).format(FMT)
