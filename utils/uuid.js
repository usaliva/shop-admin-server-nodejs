// 生成 uuid
const uuid = require('uuid')

module.exports = () => {
  return uuid.v1()
}
