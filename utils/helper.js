// 组织树型结构的数据
exports.getTree = arr => {
  const list = []
  const obj = {}
  arr.forEach(v => {
    obj[v.id] = v
  })

  arr.forEach(v => {
    if (v.pid === 0) {
      list.push(v)
      return
    }

    // 无顶级菜单的垃圾数据
    if (!obj[v.pid]) return

    const parent = obj[v.pid]

    if (!parent.children) {
      parent.children = []
    }
    parent.children.push(v)
  })

  return list
}
