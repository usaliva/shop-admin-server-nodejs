/*  静态常量 */

// 状态码
exports.STATUS = Object.freeze({
  SUCCESS: 200, // 成功
  FAILED: 400, // 失败
  UNAUTHORIZED: 401, // 无操作权限
  LOGIN_CAPTCHA_EXPIRED: 410000 // 登录验证码过期
})

// 表名
exports.TABLE = Object.freeze({
  USER: 'user',
  ROLE: 'role',
  MENU: 'menu',
  PRODUCT: 'product',
  CATEGORY: 'category'
})
