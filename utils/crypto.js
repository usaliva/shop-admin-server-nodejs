// 加密解密方法
const uuid = require('./uuid')
const crypto = require('crypto')

const cryptoPwd = (str, salt) => {
  if (str.length === 0) throw new Error('未提供加密的内容')

  salt = salt || uuid()
  const keyBuffer = crypto.pbkdf2Sync(
    str,
    salt,
    1000, // 迭代次数，次数越多暴露破解越困难
    16, // 要生成的 key 的字节长度
    'sha512' // 使用的算法
  )

  return {
    salt,
    key: keyBuffer.toString('hex')
  }
}

module.exports = cryptoPwd
