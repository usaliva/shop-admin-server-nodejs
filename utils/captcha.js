// 生成图片验证码
const svgCaptcha = require('svg-captcha')

const captcha = () => {
  return svgCaptcha.create({
    ignoreChars: 'Oo0LlIi1', // 忽略的字符
    noise: 2, // 干扰线条数
    color: false // 文字颜色是否随机 false-黑色，true-随机（如果设置了background，则 color 强制为 true）
    // background: '#fff' // 背景颜色
  })
}

module.exports = captcha
