// token 相关
const jwt = require('jsonwebtoken')
const { jwtSecret } = require('../config/config.default')

exports.createToken = data => {
  return jwt.sign(data, jwtSecret, {
    expiresIn: '1d'
  })
}
