const { getTable, setTable } = require('../../database')
const { STATUS, TABLE } = require('../../utils/constants')
const { format } = require('../../utils/date')
const { getTree } = require('../../utils/helper')

// 根据 id 获取 category
async function getCategory (req) {
  const id = Number.parseInt(req.params.id)
  const categorys = await getTable(TABLE.CATEGORY)

  const category = categorys.find(v => v.id === id)

  if (!category) {
    throw new Error('分类不存在')
  }

  return { categorys, category }
}

// 获取分类列表
exports.getCategorys = async (req, res, next) => {
  let { page, limit, status } = req.query
  page = page ? Number.parseInt(page) : 1
  limit = limit ? Number.parseInt(limit) : 10
  status = ['0', '1'].includes(status) ? Number.parseInt(status) : null

  try {
    let categorys = await getTable(TABLE.CATEGORY)
    categorys = categorys.filter(v => !v.isDel)
    let list = getTree(categorys)

    // 总条数
    const count = list.length

    // 筛选 排序 截取 转化时间格式
    const sortAndFilter = arr => {
      const res = status === null ? arr : arr.filter(v => v.status === status)

      res.forEach(v => {
        v._addTime = format(v._addTime)
        v._updateTime = format(v._updateTime)
        if (v.children && v.children.length > 0) {
          v.children = sortAndFilter(v.children)
        }
      })

      return res.sort((a, b) => a.order - b.order)
    }
    list = sortAndFilter(list).slice((page - 1) * limit, page * limit)

    // 响应结果
    res.json({
      status: STATUS.SUCCESS,
      data: {
        list,
        count
      }
    })
  } catch (error) {
    next(error)
  }
}

// 添加分类
exports.createCategory = async (req, res, next) => {
  let {
    pid,
    name,
    order,
    status
  } = req.body
  pid = pid || 0

  try {
    const categorys = await getTable(TABLE.CATEGORY)

    // 校验父级是否存在
    const parent = categorys.find(v => v.id === pid)
    if (!parent || parent.isDel === 1) {
      next('上级分类不存在')
      return
    }

    // 生成 id 自增
    let id = 1
    if (categorys.length > 0) {
      const menuIds = categorys.map(v => v.id).sort()
      id = menuIds[menuIds.length - 1] + 1
    }

    const now = Date.now()

    const category = {
      id,
      pid,
      name,
      order,
      status,
      isDel: 0,
      _addTime: now,
      _updateTime: now
    }

    categorys.push(category)

    await setTable(TABLE.CATEGORY, categorys)

    // 响应结果
    res.json({
      status: STATUS.SUCCESS,
      msg: 'ok'
    })
  } catch (error) {
    next(error)
  }
}

// 修改分类
exports.updateCategory = async (req, res, next) => {
  let {
    pid = 0,
    name,
    order,
    status
  } = req.body
  pid = pid || 0

  try {
    const { categorys, category } = await getCategory(req)

    // 校验父级是否存在
    if (pid !== category.pid) {
      const parent = categorys.find(v => v.id === pid)
      if (!parent || parent.isDel === 1) {
        next('上级分类不存在')
        return
      }
    }

    category.pid = pid
    category.name = name
    category.order = order
    category.status = status
    category._updateTime = Date.now()

    await setTable(TABLE.CATEGORY, categorys)

    // 响应结果
    res.json({
      status: STATUS.SUCCESS,
      msg: 'ok'
    })
  } catch (error) {
    next(error)
  }
}

// 获取分类
exports.getCategory = async (req, res, next) => {
  try {
    const { category } = await getCategory(req)

    category._addTime = format(category._addTime)
    category._updateTime = format(category._updateTime)

    // 响应结果
    res.json({
      status: STATUS.SUCCESS,
      data: category
    })
  } catch (error) {
    next(error)
  }
}

// 删除分类
exports.deleteCategory = async (req, res, next) => {
  try {
    const { categorys, category } = await getCategory(req)

    category.isDel = 1
    category._updateTime = Date.now()
    await setTable(TABLE.CATEGORY, categorys)

    // 响应结果
    res.json({
      status: STATUS.SUCCESS,
      msg: 'ok'
    })
  } catch (error) {
    next(error)
  }
}

// 修改分类状态
exports.setCategoryStatus = async (req, res, next) => {
  const status = Number.parseInt(req.params.status)

  try {
    const { categorys, category } = await getCategory(req)

    category.status = status
    category._updateTime = Date.now()
    await setTable(TABLE.CATEGORY, categorys)

    // 响应结果
    res.json({
      status: STATUS.SUCCESS,
      msg: 'ok'
    })
  } catch (error) {
    next(error)
  }
}
