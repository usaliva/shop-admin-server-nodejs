const captcha = require('../utils/captcha')
const { getTable, setTable, getUnDelData } = require('../database')
const cryptoPwd = require('../utils/crypto')
const { createToken } = require('../utils/jwt')
const { STATUS, TABLE } = require('../utils/constants')
const { getTree } = require('../utils/helper')

// 测试
exports.demo = async (req, res, next) => {
  try {
    res.json({
      status: STATUS.SUCCESS,
      data: {
        title: 'Hello World',
        date: Date.now()
      }
    })
  } catch (error) {
    next(error)
  }
}

// 解析 token
exports.token = async (req, res, next) => {
  console.log(req.auth)
}

// 登录验证码
exports.captcha = (req, res, next) => {
  const loginCaptcha = captcha()
  req.session.loginCaptcha = loginCaptcha.text

  res.type('svg')
  res.status(STATUS.SUCCESS).send(loginCaptcha.data)
}

// 管理员登录
exports.login = async (req, res, next) => {
  const { account, pwd, imgcode } = req.body

  // 跳过验证码校验（内部使用）
  if (imgcode !== '0000') {
    // 图片验证码校验
    if (!req.session.loginCaptcha) {
      return res.json({
        status: STATUS.LOGIN_CAPTCHA_EXPIRED,
        msg: '验证码失效'
      })
    } else if (imgcode.toUpperCase() !== req.session.loginCaptcha.toUpperCase()) {
      return res.json({
        status: STATUS.FAILED,
        msg: '验证码错误'
      })
    }
  }

  try {
    const users = await getTable(TABLE.USER)
    const user = users.find(v => v.account === account)

    if (!user) {
      throw new Error('用户不存在')
    }

    const { salt, password, id, realName } = user

    // 密码校验
    const { key } = cryptoPwd(pwd, salt)
    if (key !== password) {
      throw new Error('密码错误')
    }

    // 生成token
    const token = createToken({ id: id })

    // 获取权限
    let menus = await getUnDelData(TABLE.MENU)
    const uniqueAuth = []

    // 超级管理员获取所有权限，其他用户根据角色获取
    if (user.id !== 1) {
      const roles = await getUnDelData(TABLE.ROLE)
      let roleMenus = []
      roles.forEach(v => {
        if (v.status === 1 && !user.roles.includes(v.id)) return
        roleMenus.push(v.menus)
      })
      roleMenus = Array.from(new Set(roleMenus))

      menus = menus.filter(v => v.status === 1 && roleMenus.includes(v.id))
    }

    // 组织树型结构
    menus = getTree(menus)

    // 排序
    const sort = (arr = []) => {
      const res = []
      arr.forEach(v => {
        if (v.uniqueAuth) {
          uniqueAuth.push(v.uniqueAuth)
        }

        if (v.isHidden === 0) {
          res.push({
            id: v.id,
            routePath: v.path + v.params,
            routeName: v.uniqueAuth,
            name: v.name,
            icon: v.icon,
            children: sort(v.children || [])
          })
        }
      })

      return res.sort((a, b) => a.order - b.order)
    }

    menus = sort(menus)

    // 更新登录记录
    user._lastIp = req.ip
    user._lastTime = Date.now()
    await setTable(TABLE.USER, users)

    // 响应结果
    res.json({
      status: STATUS.SUCCESS,
      data: {
        token,
        userInfo: { id, account, realName },
        menus,
        uniqueAuth
      }
    })
  } catch (error) {
    next(error)
  }
}
