const path = require('path')

/**
 * UUID 在线生成：https://www.uuidgenerator.net/
 */
module.exports = {
  // express-session 密钥
  sessionSecret: '847bb884-76a5-4829-96b9-039d27fff091',
  // 数据库存储路径
  dbDir: path.join(__dirname, '../db'),
  // 生成 token 的密钥
  jwtSecret: '55192e46-e4a5-4f2f-bedf-a6b7b3952d3c',
  // port
  serverPort: 5020
}
